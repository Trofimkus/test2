﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void gogo_Click(object sender, EventArgs e)
        {
            while (Visible)
            {
                for (int c = 0; c < 253 && Visible; c++)
                {
                    BackColor = Color.FromArgb(c, 255 - c, c);
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(4);
                }
                for (int c = 254; c > 0 && Visible; c--)
                {
                    BackColor = Color.FromArgb(c, 255 - c, c);
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(4);
                }

            }
        }

        private void Stop_Click(object sender, EventArgs e)
        {
           
        }
    }
}
